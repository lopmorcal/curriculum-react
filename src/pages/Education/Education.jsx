import React from "react";
import "./Education.scss";
import { CV } from "../../CV/CV";

const Education = () => {
  const { education } = CV;
  console.log(education);

  return (
    <div className="education">
      <div className="title-education">
        <h1>Formación Académica</h1>
      </div>
      <div className="education-data">
        {education.map((item) => {
          return (
            <>
            <hr/>
              <p>
                <b>Formación:</b> {item.name}
              </p>
              <p>
                <b>Año:</b> {item.date}
              </p>
              <p>
                <b>Cursado en:</b> {item.where}
              </p>
              <br />
              <hr/>
            </>
          );
        })}
      </div>
    </div>
  );
};

export default Education;
