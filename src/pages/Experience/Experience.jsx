import React, { useState } from 'react';
import './Experience.scss';
import { CV } from '../../CV/CV';

const Experience = () => {

  const {experience}=CV
  const {tecnologias,hosteleria,logistica,varios}=experience

 
  const [showTec,setShowTec]=useState(false)
  const [showHost, setShowHost] = useState(false)
  const [showLog, setShowLog] = useState(false)
  const [showVarios, setShowVarios] = useState(false)
  
  const changeStateTec=()=>{
    setShowTec(!showTec)
    setShowHost(false)
    setShowLog(false)
    setShowVarios(false)
  }
  const changeStateHost=()=>{
    setShowHost(!showHost)
    setShowTec(false)
    setShowLog(false)
    setShowVarios(false)
  }
  const changeStateLog=()=>{
    setShowLog(!showLog)
    setShowHost(false)
    setShowTec(false)
    setShowVarios(false)
  }
  const changeStateVarios=()=>{
    setShowVarios(!showVarios)
    setShowHost(false)
    setShowLog(false)
    setShowTec(false)
  }

  return(
    <div className='experience'>

      <div className='experience-title'><h1>Experiencia Laboral</h1></div>
      <div className='experiende-zone'>
      <button onClick={changeStateTec} className='navButtonsExp'>Tecnologías</button>
      <button onClick={changeStateHost} className='navButtonsExp'>Hostelería</button>
      <button onClick={changeStateLog} className='navButtonsExp'>Logística</button>
      <button onClick={changeStateVarios} className='navButtonsExp'>Varios</button>
      </div>
      <div className='experience-data'> {showTec ===true &&
        <div>
         {tecnologias.map(({name,date,where,description},item)=>{
          return(
            <div key={item}>
              <hr/>
               <p><b>Puesto:</b>  {name}</p>
               <p><b>Fecha:</b>   {date}</p>
               <p><b>Empresa:</b>  {where}</p>
               <br/>
               <p><b>Funciones:</b> {description}</p>
               <br/>
              <hr/>
            </div>
                )
              })}
      </div>}
      {showHost ===true &&
        <div>
         {hosteleria.map(({name,date,where,description},item)=>{
          return(
            <div key={item}>
              <hr/>
               <p><b>Puesto:</b>  {name}</p>
               <p><b>Fecha:</b>   {date}</p>
               <p><b>Empresa:</b>  {where}</p>
               <br/>
               <p><b>Funciones:</b> {description}</p>
               <br/>
              <hr/>
            </div>
                )
              })}
      </div>}
      {showLog ===true &&
        <div>
         {logistica.map(({name,date,where,description},item)=>{
          return(
            <div key={item}>
              <hr/>
               <p><b>Puesto:</b>  {name}</p>
               <p><b>Fecha:</b>   {date}</p>
               <p><b>Empresa:</b>  {where}</p>
               <br/>
               <p><b>Funciones:</b> {description}</p>
               <br/>
              <hr/>
            </div>
                )
              })}
      </div>}
      {showVarios ===true &&
        <div>
         {varios.map(({name,date,where,description},item)=>{
          return(
            <div key={item}>
              <hr/>
               <p><b>Puesto:</b>  {name}</p>
               <p><b>Fecha:</b>   {date}</p>
               <p><b>Empresa:</b>  {where}</p>
               <br/>
               <p><b>Funciones:</b> {description}</p>
               <br/>
              <hr/>
            </div>
                )
              })}
      </div>}
      
    </div>  
    </div>
      )
};

export default Experience;

