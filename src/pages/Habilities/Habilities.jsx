import React from 'react';
import './Habilities.scss';
import { CV } from "../../CV/CV";



const Habilities = () => {

  const {habilities}=CV

  return( 
  <div className='habilities'>
    <div>
      <h1>Skills</h1>
    </div>
    <div className='habilities-data'>
     {habilities.map((item)=>{
       return(
         
          <div className='section-data'>
            <hr />
            <h3 className='title-skill'>{item.category}</h3>
            <p>{item.lang}</p>
            <hr />
            
          </div>
       )
     })}
    </div>
    </div>
    )
};

export default Habilities;
