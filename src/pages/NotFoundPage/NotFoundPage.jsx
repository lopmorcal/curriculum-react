import React from 'react';
import'./NotFoundPage.scss';

const NotFoundPage = () => {
  return(
  <div className='errorPage'>
<h1>ERROR 404 Página no encontrada</h1>
<img className='imgRabbit' src="/assets/conejo.png" alt="" />
<h1>Continúa buscando al conejo blanco...</h1>
  </div>
  );
};

export default NotFoundPage;
