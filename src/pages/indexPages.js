import Education from "./Education/Education"
import Experience from "./Experience/Experience"
import Habilities from "./Habilities/Habilities"
import Interesting from "./Interesting/Interesting"
import NotFoundPage from "./NotFoundPage/NotFoundPage"
import Home from "./Home/Home"



export{

    Education,
    Interesting,
    Habilities,
    Experience,
    Home,
    NotFoundPage,
}