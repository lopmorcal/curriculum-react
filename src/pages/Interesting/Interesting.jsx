import React from "react";
import "./Interesting.scss";
import { CV } from "../../CV/CV";

const Interesting = () => {
  const { interesting } = CV;

  return (

    <div className="interesting">
      <div><h1>Más información</h1></div>
      <div className="interesting-data">
        {interesting.map((item) => {
          return (
            <div className=" interesting-data" key={item}>
            <p><b>Carnet de conducir</b>  {item.DriveLicense}</p>
            <br />
            <p><b>Vehículo Propio</b>  {item.Car}</p>
            <br />
            <p><b>Disponibilidad</b>  {item.availability}</p>
            <br />
            <p><b>Linkedin:  </b><a href="https://www.linkedin.com/in/carlos-lopez-moreno-069b46208/"><img className="linkedin-img" src={item.link} alt="Linkedin" /></a></p>
            <br />
            <p><b>Lugar de residencia: </b>{item.Place}</p>
            </div>
          );
        })}
      </div>
    
    </div>
  );
};

export default Interesting;


