import React from 'react';
// import {Routes,Route,} from "react-router-dom";
// import routes from '../../config/routes';
import './Header.scss';
import HeaderTop from './HeaderTop/HeaderTop';
import HeaderBottom from './HeaderBottom/HeaderBottom';
import { CV } from '../../CV/CV';

const {myInfo} = CV;

const Header = () => {
    
    return( 
        <div className='header'>
            <HeaderTop myInfo={myInfo}/>
            <HeaderBottom myInfo={myInfo}/>
        </div>
        );
};

export default Header;
