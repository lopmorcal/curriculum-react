import React from 'react';
import './HeaderTop.scss';


const HeaderTop = ({myInfo}) => {
    return( 
         <div className='header-top'>
                <div className='header-top__img'></div>
                
                <div className='header-top__data'>
                    
                    <p>👦  {myInfo.name}</p>
                    <br/>
                    <p>📧<a href={"mailto:" + myInfo.email}>  clprogramer2020@ gmail .com</a></p>
                    <br/>
                    <p>📱  {myInfo.phone}</p>
                    <br/>
                    <p>💾<a href={myInfo.gitlab}>  Gitlab</a></p>
                    <br/>
                    <p>📆  {myInfo.birthDate}</p>
                    <br/>
                </div>

            </div>
        )
};

export default HeaderTop;
