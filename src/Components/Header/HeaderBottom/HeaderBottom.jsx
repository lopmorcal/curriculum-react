import React, { useState } from "react";
import "./HeaderBottom.scss";
import { Link } from "react-router-dom";


const HeaderBottom = ({ myInfo }) => {
  const [showInfo, setShowInfo] = useState(false);
  const showInfoFn = () => {
    setShowInfo(!showInfo);
  };

  return (
    <div className="header-bottom">
      <div className="header-bottom__buttoms">
        <button className="button">
          <Link to="/experiencia">Experiencia Laboral</Link>
        </button>
        <button className="button">
          <Link to="/formacion">Formación</Link>
        </button>
        <button className="button">
          <Link to="/habilidades">Skills</Link>
        </button>
        <button className="button">
          <Link to="info">Más Info</Link>
        </button>
      </div>

      <div className="header-bottom__aboutMe">
        <div onClick={()=>showInfoFn()} className="me">
          <h2>Sobre mí</h2>
        </div>
        {showInfo &&
        <div className="info-me">  
          {myInfo.aboutMe.map((item) => {
            return (
              <div key={JSON.stringify(item)}>
                <br />
                <p>{item.info}</p>
                <br />
              </div>
            );
          })}
        </div>
        }
      </div>
    </div>
  );
};

export default HeaderBottom;
