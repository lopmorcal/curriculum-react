import React from "react";
import "./Body.scss";
import {Routes,Route,} from "react-router-dom";
import routes from '../../config/routes';
//efecto matrix
var c = document.getElementById("c");
var ctx = c.getContext("2d");

c.height = window.innerHeight;
c.width = window.innerWidth;

var matrix =
  "01101101 01100101 01101110 01110011 01100001 01101010 01100101 00100000 01101111 01100011 01110101 01101100 01110100 01101111 00111010 00100000 01100101 01110011 01110000 01100101 01110010 01101111 00100000 01110001 01110101 01100101 00100000 01110100 01100101 01101110 01100111 01100001 01110011 00100000 01110101 01101110 00100000 01100010 01110101 01100101 01101110 00100000 01100100 01101001 01100001 00100000 ";

matrix = matrix.split("");

var font_size = 10;
var columns = c.width / font_size;

var drops = [];

for (var x = 0; x < columns; x++) drops[x] = 1;

function draw() {
  ctx.fillStyle = "rgba(0, 0, 0, 0.04)";
  ctx.fillRect(0, 0, c.width, c.height);

  ctx.fillStyle = "#34D848";
  ctx.font = font_size + "px arial";

  for (var i = 0; i < drops.length; i++) {
    var text = matrix[Math.floor(Math.random() * matrix.length)];

    ctx.fillText(text, i * font_size, drops[i] * font_size);

    if (drops[i] * font_size > c.height && Math.random() > 0.975) drops[i] = 0;

    drops[i]++;
  }
}

setInterval(draw, 35);
//fin efecto matrix

const Body = () => {
  return (
    <div className="body">
      
      <div className="card">
      <Routes>
        {routes.map(({ path, element }) => {
          return <Route key={path} path={path} element={element} />;
        })}
      </Routes>
      </div>
    </div>
  );
};

export default Body;
