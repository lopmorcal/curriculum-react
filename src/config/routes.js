import { Education,Experience,Habilities,Interesting,Home,NotFoundPage } from "../pages/indexPages"



const routes=[

    {
        path:'/',
        element:<Home/>,
    },
    {
        path:'/formacion',
        element:<Education/>,
    },
    {
        path:'/experiencia',
        element:<Experience/>,
    },
    {
        path:'/habilidades',
        element:<Habilities/>,
    },
    {
        path:'/info',
        element:<Interesting/>,
    },
    {
        path:'/*',
        element:<NotFoundPage/>,
    },
];

export default routes