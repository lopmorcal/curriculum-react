
import './App.scss';
import { Body, Header } from './Components';

function App() {
  return (
    <div className='app' >

      <Header/>
      <Body/>      
      
    </div>
  );
}

export default App;
