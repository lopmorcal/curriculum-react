export const CV = {
  myInfo: {
    name: "Carlos Lopez Moreno",
    city: "Guadalajara / España",
    email: "clprogramer2020@gmail.com",
    birthDate: "29/08/1988",
    phone: "665464355",
    image: "https://i.imgur.com/ZQAkED3.png",
    gitlab: "https://gitlab.com/lopmorcal",
    aboutMe: [
      {
        info: "🔩 Developer Junior Full Stack",
      },
      {
        info: "🤖 El esfuerzo constante – no la fuerza o la inteligencia – es la clave para liberar nuestro potencial.",
      },
      {
        info: "🕶 Ser trabajador, constante, detallista son cualidades que me caracterizan.",
      },
      {
        info: "🦾 Actualmente, me encuentro buscando una empresa que me dé la oportunidad de avanzar juntos laboralmente y poder crecer como desarrollador.",
      },
    ],
  },
  education: [
    {
      name: "Desarrollador Full Stack",
      date: "2020-2021",
      where: "Upgrade Hub",
    },
    {
      name: "CSS3, HTML 5,  Maquetación web, Responsive Web Design, SASS, Flexbox, CSS Grid, Bootstrap 4 y 5.",
      date: "2020",
      where: "Udemy, Victor Robles",
    },
    {
      name: "Metodologías de trabajo Agile y Scrum.",
      date: "2020-2021",
      where: "Upgrade Hub",
    }
  ],
  experience:{

    tecnologias:[
      {
        name: "Desarrollador Full Stack",
        date: "Junio 2022 hasta la actualidad",
        where: "Orange España / Jazzplat",
        description:
          "Desarrollo modular de aplicación interna para gestión de horarios de empleados de Jazzplat, gestión de informes y tratamiento de datos a mostrar en la aplicación corporativa y solución de errores/modificación de esta misma app",
      },
    {
      name: "Servicio técnico de redes",
      date: "2018 hasta junio 2022",
      where: "Orange España / Jazzplat",
      description:
        "Tareas de reparación telemática con el cliente y resolución de problemas técnicos con la linea ADSL/FTTH/movil, gestión de incidencias tanto internas como externas, trabajo en equipo coordinado con departamentos.",
    },
    {
      name: "Teleoperador anti-cancelaciones ",
      date: "01/06/2015 – 01/02/2018",
      where: "Orange España / Jazzplat",
      description:
        "Gestión de soluciones en incidencias con portabilidades de compañía, trato con cliente y tareas de retención coordinadas en equipo.",
    }
  ],
  hosteleria:[
    {
      name: "Ayudante de cocina ",
      date: "Septiembre 2009",
      where: "Restaurante La Carrera, Guadalajara.",
      description:
        "Preparación de platos fríos y elaboraciones básicas para el servicio.",
    },
    {
      name: "Ayudante de cocina",
      date: " 2011",
      where: " Restaurante Los Arcos, Cuenca",
      description:
        "Preparación de platos fríos y elaboraciones básicas para el servicio.",
    },
    {
      name: "Cocinero, camarero y mantenimiento de instalaciones",
      date: " 2012/2013",
      where: "Hostal Alto Tajo, Guadalajara",
      description:
        "Elaboración de menús preparación y emplatado, servicio en mesas y barra mantenimiento de instalaciones.",
    },
    {
      name: "RRPP/Camarero",
      date: " 2012/2013",
      where: "Discoteca Caché Cuenca y Pub La Calle",
      description:
        "Servicio de bebidas y refrescos, captación de clientes.",
    }
  ],
  logistica:[
    {
      name: "Mozo de almacén",
      date: "2004",
      where: "C&A",
      description:
        "Preparación de pedidos y empaquetado.",
    },
    {
      name: "Carretillero",
      date: "2005/2006",
      where: "Manipulados Polo",
      description:
        "Preparación de pedidos con carretilla eléctrica, manual y retractiles.",
    },
    {
      name: "Manipulador-mozo de almacén",
      date: "2009/2010",
      where: "Fripan",
      description:
        "Preparación de pedidos con carretilla retráctil y manipulación alimentos precongelados.",
    }
  ],
  varios:[
    {
      name: "Jardinero-albañil",
      date: "2010/2011",
      where: "Ayuntamiento Guadalajara ",
      description:
        "Preparación de terreno, montaje de sistema de regadío, cuidado y mantenimiento fitosanitario de plantas.",
    },
    {
      name: "Carpintería montador de muebles",
      date: "2012/2013",
      where: "Ayuntamiento Cuenca",
      description:
        "Preparación y fabricación de muebles, montaje y transporte.",
    },
    {
      name: "Promotor/vendedor de accesorios de móviles",
      date: "2014/2015",
      where: "Centro comercial Ferial plaza",
      description:
        "Venta cara al público accesorios de móviles.",
    },
    {
      name: "Comercial seguros",
      date: "2012/2013",
      where: "Seguros Ocaso",
      description:
        "Venta cara al público Seguros Ocaso.",
    },
    {
      name: "Promotor/vendedor Movistar",
      date: "2009",
      where: "Eroski Guadalajara",
      description:
        "Venta cara al público Telefonía móvil marca Movistar.",
    },
    {
      name: "Promotor/vendedor Orange",
      date: "2007/2008",
      where: "Eroski Guadalajara",
      description:
        "Venta cara al público Telefonía móvil marca Orange.",
    }
  ],
 },
 
  habilities: [
    { 
      category:"Frontend",
      lang: "HTML, CSS, JavaScript, React, Angular",
      
    },

    { 
      category:"Backend",
      lang: "NodeJS, Express, Mongo" 
    },
    { 
      category: "Control Versions",
      lang: "Git" 
    },
    {
      category:"Soft Skills",
      lang:"Metodologías de trabajo Agile y Scrum, trabajo en equipo, dinamico y proactivo",
    },
  ],
  interesting:[{
    DriveLicense: "Si",
    Car: "Si",
    availability: "Consultar",
    link:"https://www.freeiconspng.com/thumbs/linkedin-logo-png/linkedin-logo-0.png",
    Place:"Guadalajara/España"
  }],
};
